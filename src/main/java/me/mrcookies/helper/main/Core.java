package me.mrcookies.helper.main;

import me.mrcookies.helper.configuration.ConfigManager;
import me.mrcookies.helper.database.MySQL;
import me.mrcookies.helper.listeners.ListenerManager;
import me.mrcookies.helper.listeners.events.BotStartEvent;
import me.mrcookies.helper.listeners.events.BotStatusEvent;
import me.mrcookies.helper.redeem.MessageRedeemEvent;
import me.mrcookies.helper.rules.ReactionStartEvent;
import me.mrcookies.helper.tickets.HelpMessageEvent;
import me.mrcookies.helper.utils.Methods;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class Core {

    private static JDA jda;
    private static ConfigManager config;
    private static MySQL mysql;

    public static void main(String[] args) throws Exception {
        setupConfig();
        setupFolders();

        if (config.getYml().getString("Settings.token").isEmpty()) {
            System.out.println("Helper > NO BOT TOKEN FOUND! Cannot start :C");
            return;
        }

        mysql = new MySQL();

        mysql.initialize();

        System.out.println("Helper > Database ready.");

        jda = new JDABuilder(AccountType.BOT).setToken(config.getYml().getString("Settings.token"))
                .addEventListeners(new BotStartEvent(), new ReactionStartEvent(), new BotStatusEvent(), new HelpMessageEvent(), new MessageRedeemEvent())
                .build()
                .awaitReady();

        new ListenerManager();

        jda.getRateLimitPool().scheduleWithFixedDelay(() -> jda.getPresence().setActivity(Activity.streaming(getRandomActivity(), "https://www.twitch.tv/chill24h")), 0, 5, TimeUnit.SECONDS);

        System.out.println("Helper > Bot ready to use.");
    }

    private static void setupConfig() {
        config = new ConfigManager("config", "Helper");
        config.addDefault("Prefix", "");
        config.addDefault("Settings.token", "");
        config.addDefault("Server.ip", "");
        config.addDefault("Server.port", "");
        config.addDefault("Roles.permission", "");
        config.addDefault("Roles.helper", "");
        config.addDefault("Roles.utility", "");
        config.addDefault("Reactions.like", "");
        config.addDefault("Reactions.dislike", "");
        config.addDefault("Reactions.check", "");
        config.addDefault("Roles.no-tag", "");
        config.addDefault("Games.Countgame.channel", "");
        config.addDefault("Games.Countgame.surprise", 10);
        config.addDefault("Games.Countgame.reference", 1);
        config.addDefault("Games.Countgame.last-author", "");
        config.addDefault("Games.QuickMath.channel", "");
        config.addDefault("Games.QuickMath.reference", "");
        config.addDefault("Games.HigherLower.channel", "");
        config.addDefault("Games.HigherLower.reference", "");
        config.addDefault("MySQL.host", "");
        config.addDefault("MySQL.port", "");
        config.addDefault("MySQL.database", "");
        config.addDefault("MySQL.username", "");
        config.addDefault("MySQL.password", "");
        config.addDefault("Channels.support-channel", "");
        config.addDefault("Roles.support", "");
        config.addDefault("Roles.muted", "");
        config.addDefault("Roles.member", "");
        config.addDefault("Roles.support", "");
        config.addDefault("Roles.new", "");
        config.addDefault("Messages.rules", "");
        config.addDefault("Channels.command-channel", "");
        config.addDefault("Channels.logs-channel", "");
        config.addDefault("Channels.command-channel-staff", "");
        config.addDefault("Channels.redeem-channel", "");
        config.addDefault("Channels.status-channel", "");
        config.addDefault("Channels.tickets-channel", "");
        config.addDefault("Channels.rules-channel", "");
        config.addDefault("Channels.giveaways-channel", "");
        config.addDefault("Channels.announcements-channel", "");
        config.addDefault("Reactions.online", "");
        config.addDefault("Reactions.offline", "");
        config.addDefault("Reactions.loading", "");
        config.addDefault("Category.tickets", "");
        config.addDefault("Channels.requests-channel", "");
        config.saveDefaults();
    }

    private static String getRandomActivity() {
        String[] activities = {
                Methods.getUsersSize() + " USERS",
                "MY CODE",
                "VIDEOS",
                "DISCORD",
                "MUSIC"
        };

        int ran = Methods.getRandom(activities.length, 0);

        return activities[ran];
    }

    private static void setupFolders() {

        if (!new File("Helper/Images").exists()) {
            new File("Helper/Images").mkdir();
        }

        if (!new File("Helper/Chatlogs").exists()) {
            new File("Helper/Chatlogs").mkdir();
        }

    }

    public static ConfigManager getConfig() {
        return config;
    }

    public static String getVersion() {
        return "2.6.0";
    }

    public static JDA getJDA() {
        return jda;
    }

    public static MySQL getMySQL() {
        return mysql;
    }

}
